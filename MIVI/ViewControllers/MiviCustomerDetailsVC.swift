//
//  MiviCustomerInfoVC.swift
//  MIVI
//
//  Created by Trilok on 8/7/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

var totaladata:CollectDataVal!
import UIKit
import Foundation

let collectDataValues = """
{
  "data": {
    "type": "accounts",
    "id": "2593177",
    "attributes": {
      "payment-type": "prepaid",
      "unbilled-charges": null,
      "next-billing-date": null,
      "title": "Ms",
      "first-name": "Joe",
      "last-name": "Bloggs",
      "date-of-birth": "1985-01-01",
      "contact-number": "0404000000",
      "email-address": "test@mivi.com",
      "email-address-verified": false,
      "email-subscription-status": false
    },
    "links": {
      "self": "http://localhost:3000/accounts/2593177"
    },
    "relationships": {
      "services": {
        "links": {
          "related": "http://localhost:3000/services/0468874507"
        }
      }
    }
  },
  "included": [
    {
      "type": "services",
      "id": "0468874507",
      "attributes": {
        "msn": "0468874507",
        "credit": 1200,
        "credit-expiry": "2016-11-20",
        "data-usage-threshold": false
      },
      "links": {
        "self": "http://localhost:3000/services/0468874507"
      },
      "relationships": {
        "subscriptions": {
          "links": {
            "related": "http://localhost:3000/services/0468874507/subscriptions"
          },
          "data": [
            {
              "type": "subscriptions",
              "id": "0468874507-0"
            }
          ]
        }
      }
    },
    {
      "type": "subscriptions",
      "id": "0468874507-0",
      "attributes": {
        "included-data-balance": 52400,
        "included-credit-balance": null,
        "included-rollover-credit-balance": null,
        "included-rollover-data-balance": null,
        "included-international-talk-balance": null,
        "expiry-date": "2016-11-19",
        "auto-renewal": true,
        "primary-subscription": true
      },
      "links": {
        "self": "http://localhost:3000/services/0468874507/subscriptions/0468874507-0"
      },
      "relationships": {
        "service": {
          "links": {
            "related": "http://localhost:3000/services/0468874507"
          }
        },
        "product": {
          "data": {
            "type": "products",
            "id": "0"
          }
        },
        "downgrade": {
          "data": null
        }
      }
    },
    {
      "type": "products",
      "id": "4000",
      "attributes": {
        "name": "UNLIMITED 7GB",
        "included-data": null,
        "included-credit": null,
        "included-international-talk": null,
        "unlimited-text": true,
        "unlimited-talk": true,
        "unlimited-international-text": false,
        "unlimited-international-talk": false,
        "price": 3990
      }
    }
  ]
}
""".data(using: .utf8)! // our data in native (JSON) format


class MiviCustomerDetailsVC: UIViewController {
    
    var customerPopup : MiviCustomerInfoPopupVC!
    var productDetailsPopUp : MiviProductDetailsPopUpVC!
    var subscriptionPopUp : MiviSubscriptionPopUpVC!

    @IBOutlet weak var msnNumber: UILabel!
    @IBOutlet weak var firstnameLabel: UILabel!
    
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        do {
            let collectDataObj = try JSONDecoder().decode(CollectDataVal.self, from: collectDataValues) // Decoding our data
            totaladata = collectDataObj
            firstnameLabel.text = collectDataObj.data.attributes.firstName
            lastNameLabel.text = collectDataObj.data
            .attributes.lastName
            
            if let msnObj = collectDataObj.included.first?.attributes?.msn,let creditObj = collectDataObj.included.first?.attributes?.credit
            {
            msnNumber.text = msnObj
                creditLabel.text = String(creditObj)
            }
            if let priceObj = collectDataObj.included.last?.attributes?.price,let prodctNameObj = collectDataObj.included.last?.attributes?.name
            {
                productPriceLabel.text = String(priceObj)
                productNameLabel.text = prodctNameObj
            }
          
        } catch(let error) {
            print(error)
        }
    }
    @IBAction func subscriptionPopUpButtonTapped(_ sender: Any) {
        subscriptionPopUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiviSubscriptionPopUpVC") as? MiviSubscriptionPopUpVC
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.addChildViewController(subscriptionPopUp)
        subscriptionPopUp.view.frame = self.view.frame
        self.view.addSubview(subscriptionPopUp.view)
        subscriptionPopUp.didMove(toParentViewController: self)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {

        self.navigationController?.popToRootViewController(animated: true)
   
    }
    
    @IBAction func productDetailsPopUpBtTapped(_ sender: Any) {
        
        productDetailsPopUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiviProductDetailsPopUpVC") as? MiviProductDetailsPopUpVC
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.addChildViewController(productDetailsPopUp)
        productDetailsPopUp.view.frame = self.view.frame
        self.view.addSubview(productDetailsPopUp.view)
        productDetailsPopUp.didMove(toParentViewController: self)
        
    }
    @IBAction func customerInfoPopUpBtTapped(_ sender: Any) {
        
        customerPopup = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiviCustomerInfoPopupVC") as? MiviCustomerInfoPopupVC
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.addChildViewController(customerPopup)
        customerPopup.view.frame = self.view.frame
        self.view.addSubview(customerPopup.view)
        customerPopup.didMove(toParentViewController: self)
    }
}
