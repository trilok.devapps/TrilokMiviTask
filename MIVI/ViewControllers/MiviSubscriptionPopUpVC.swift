//
//  MiviSubscriptionPopUpVC.swift
//  MIVI
//
//  Created by Trilok on 8/8/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import UIKit

class MiviSubscriptionPopUpVC: UIViewController {
    @IBOutlet weak var primarySubscriptionImageView: UIImageView!
    
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var includedDataBalLabel: UILabel!
    @IBOutlet weak var autorenualImageview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (totaladata.included[1].attributes?.autorenual)!
        {
             autorenualImageview.image = UIImage(named: "Right Click")
        }
        if (totaladata.included[1].attributes?.primarySubscription)!
        {
            primarySubscriptionImageView.image = UIImage(named: "Right Click")
        }

        if let expirydate = totaladata.included[1].attributes?.expiryDate,let includedBal = totaladata.included[1].attributes?.includedDataBal
        {
            expiryDateLabel.text = expirydate
            includedDataBalLabel.text = String(includedBal)
        }
       
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    

}
