//
//  MiviProductDetailsPopUpVC.swift
//  MIVI
//
//  Created by Trilok on 8/8/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import UIKit

class MiviProductDetailsPopUpVC: UIViewController {

    @IBOutlet weak var unlimitedInternationalTalkImageView: UIImageView!
    @IBOutlet weak var unlimitedInterNAtionalTextImage: UIImageView!
    @IBOutlet weak var unlimitedTalkImageView: UIImageView!
    @IBOutlet weak var unlimitdTextImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if (totaladata.included.last?.attributes?.unlimitedTack)!
       {
        unlimitedTalkImageView.image = UIImage(named: "Right Click")
        }
        if (totaladata.included.last?.attributes?.unlimitedText)!
       {
         unlimitdTextImageView.image = UIImage(named: "Right Click")
        }
        if (totaladata.included.last?.attributes?.unlimitedInternationalTalk)!
        {
            unlimitedInternationalTalkImageView.image = UIImage(named: "Right Click")
        }
        if (totaladata.included.last?.attributes?.unlimitedInternationalText)!
        {
            unlimitedInterNAtionalTextImage.image = UIImage(named: "Right Click")
        }
        
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
}
