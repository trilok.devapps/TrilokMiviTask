//
//  MiviCustomerInfoPopupVC.swift
//  MIVI
//
//  Created by Trilok on 8/8/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import UIKit

class MiviCustomerInfoPopupVC: UIViewController {

    @IBOutlet weak var customerPopUpview: UIView!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customerPopUpview.layer.borderColor = UIColor.white.cgColor
        customerPopUpview.layer.borderWidth = 2
        customerPopUpview.clipsToBounds = true

        emailAddressLabel.text = totaladata.data.attributes.emailAddress
        contactNumberLabel.text = totaladata.data.attributes.contactNumber
        paymentLabel.text = totaladata.data.attributes.paymentType
        fullNameLabel.text = totaladata.data.attributes.firstName + " " + totaladata.data.attributes.lastName
        dateOfBirthLabel.text = totaladata.data
        .attributes.dateOfBirth
        
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        
       self.view.removeFromSuperview()
        
        
    }
    
}
