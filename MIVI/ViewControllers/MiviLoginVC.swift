//
//  ViewController.swift
//  MIVI
//
//  Created by Trilok on 8/7/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import UIKit


class MiviLoginVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var msnNumberTextField: UITextField!
    
    @IBOutlet weak var numberTextField: UITextField!
    
   let msnNumber = "0468874507"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        msnNumberTextField.delegate = self
        msnNumberTextField.returnKeyType = .done
        addDoneButtonOnKeyboard()
        
    }

    @IBAction func skipButtontapped(_ sender: Any) {
        let con = ViewControllerManager.miviSplashVC()
        self.navigationController?.pushViewController(con, animated: false)
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.msnNumberTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.msnNumberTextField.resignFirstResponder()
    }
    @IBAction func loginButtonTapped(_ sender: Any) {
        
        if  msnNumberTextField.text != "" && msnNumberTextField.text == "0468874507"
        {

            let con = ViewControllerManager.miviSplashVC()
            self.navigationController?.pushViewController(con, animated: true)
            
        }
        else
        {
            let alert = UIAlertController(title: "MIVI", message: "Enter MSN Number Correctly as per Collection.json", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
          
        }
  
    }
    
}

