//
//  MiviSplashVC.swift
//  MIVI
//
//  Created by Trilok on 8/7/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import UIKit
import Foundation

class MiviSplashVC: UIViewController {
     let privateSerialQueue = DispatchQueue(label: "com.mivi")
     override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            sleep(1)
            let con = ViewControllerManager.miviCustomerDetailsVC()
            self.navigationController?.pushViewController(con, animated: false)
        }
    }

}
