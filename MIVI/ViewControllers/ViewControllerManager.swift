//
//  ViewControllerManager.swift
//  MIVI
//
//  Created by Trilok on 8/8/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerManager:NSObject{
    
    
    class func miviSplashVC()-> MiviSplashVC{
        let con = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiviSplashVC") as! MiviSplashVC
        return con
    }
    class func miviCustomerDetailsVC() ->MiviCustomerDetailsVC{
        let con = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiviCustomerDetailsVC") as! MiviCustomerDetailsVC
        return con
    }
    class func miviLoginVC() ->MiviLoginVC{
        let con = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiviLoginVC") as! MiviLoginVC
        return con
    }
    
}
