//
//  MiviModel.swift
//  MIVI
//
//  Created by Trilok on 8/7/18.
//  Copyright © 2018 DATAPPS. All rights reserved.
//

import Foundation

struct CollectDataVal: Codable
{
     var data : Data
     var included: [Included]

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decode(Data.self, forKey: .data)
        included = try container.decode([Included].self, forKey: .included)
    }
}

struct Data : Codable {
    var type : String
    var id :String
    var attributes : DataAttributes
}

struct DataAttributes:Codable {
    var title : String
    var paymentType:String
    var firstName:String
    var lastName:String
    var dateOfBirth:String
    var emailAddress :String
    var contactNumber :String
    
    private enum CodingKeys:String,CodingKey
    {
        case title
        case paymentType = "payment-type"
        case firstName = "first-name"
        case lastName = "last-name"
        case dateOfBirth = "date-of-birth"
        case emailAddress = "email-address"
        case contactNumber = "contact-number"
    }
}

struct Included : Codable{
    var type: String
    var id: String
    var attributes: IncludedAttributes?
}

struct IncludedAttributes:Codable
{
    var includedDataBal : Int?
    var expiryDate:String?
    var name:String?
    var price:Int?
    var id : String?
    var msn : String?
    var unlimitedText: Bool?
    var unlimitedTack : Bool?
    var unlimitedInternationalText : Bool?
    var unlimitedInternationalTalk : Bool?
    var credit:Int?
    var autorenual:Bool?
    var primarySubscription:Bool?

    private enum CodingKeys:String,CodingKey
    {
        case name
        case price
        case id
        case msn
        case credit
        case includedDataBal = "included-data-balance"
        case expiryDate = "expiry-date"
        case unlimitedText = "unlimited-text"
        case unlimitedTack = "unlimited-talk"
        case unlimitedInternationalText = "unlimited-international-text"
        case unlimitedInternationalTalk = "unlimited-international-talk"
        case autorenual = "auto-renewal"
        case primarySubscription = "primary-subscription"
    }
}



